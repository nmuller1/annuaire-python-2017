""" Projet 4 : Annuaire de super-héros"""

__author__ = "Noëmie Muller"
__matricule__ = "000458865"
__date__ = "02/12/2017"
__cours__ = "info-f-101"
__titulaire__ = "Thierry Massart"
__groupe_tp_ = "4"

import sys

def read_data(filename) :
	"""
	Lit le fichier contenant les informations sur les super-héros et 
	le convertit en un dictionanire (= annuaire).

	"""
	with open("example_annuaire.txt", 'r', encoding = "utf-8") as fichier :
		annuaire = {}
		for line in fichier : #passe en revue les lignes du fichier
			donnees = line.strip().split(';')  #sépare les données du super-héros et les met dans une liste
			super_heros = donnees[0] 
			ville = donnees[1]
			telephone = donnees[2]
			supers_pouvoirs = donnees[3].split(',')
			puissance = donnees[4]
			if not ville in annuaire : 
				annuaire[ville] = {} #création dictionnaire de la ville
			annuaire[ville][super_heros] = {} #création dictionnaire du super-héros
			annuaire[ville][super_heros]['telephone'] = telephone
			annuaire[ville][super_heros]['supers pouvoirs'] = supers_pouvoirs
			annuaire[ville][super_heros]['puissance'] = int(puissance)
	return annuaire

def write_data(annuaire) :
	"""
	Sauvegarde l'annuaire dans un fichier .txt.

	"""
	filename = input("Dans quel fichier voulez vous sauvegarder l'annuaire ? ")
	while filename[-4:] != '.txt' :
		print("Le nom du fichier doit se terminer par '.txt'.")
		filename = input("Dans quel fichier voulez vous sauvegarder l'annuaire ? ")
	with open(filename,'w') as fichier :
		for ville in annuaire :
			for hero in annuaire[ville] :
				telephone = annuaire[ville][hero]['telephone']
				supers_pouvoirs = ','.join(annuaire[ville][hero]['supers pouvoirs'])
				puissance = str(annuaire[ville][hero]['puissance'])
				fichier.write(hero+';'+ville+';'+telephone+';'+supers_pouvoirs+';'+puissance+'\n')
	print(filename.isclosed())

def add_hero(annuaire) :
	"""
	Ajoute un nouveau super-héros dans l'annuaire.

	"""
	super_heros = input("Comment s'appelle votre super-héro ? ")
	ville = input("Dans quelle ville habite-t-il ? ")
	telephone = input("Quel est son numéro de téléphone ? ")
	while not check_phone(telephone) :
		print("Votre numéro de téléphone doit être composé de 3 entiers séparés par '-'")
		print("dans le format (CCC)-CCC-CCCC. Veuillez réessayer d'entrer le numéro.")
		telephone = input("Quel est son numéro de téléphone ? ")
	supers_pouvoirs = []
	pouvoir = input("Quels sont ses super-pouvoirs ? (Entrez 0 pour arrêter l’enregistrement)\n")
	while pouvoir != '0' :
		supers_pouvoirs.append(pouvoir)
		pouvoir = input()
	puissance = input("Quelle est sa puissance ? ")
	while not check_puissance(puissance) :
		print("Vous devez rentrer un nombre entier entre 0 et 100 !")
		puissance = input("Quelle est sa puissance ? ")
	if not ville in annuaire : 
				annuaire[ville] = {} #création dictionnaire de la ville
	annuaire[ville][super_heros] = {} #création dictionnaire du super-héros
	annuaire[ville][super_heros]['telephone'] = telephone
	annuaire[ville][super_heros]['supers pouvoirs'] = supers_pouvoirs
	annuaire[ville][super_heros]['puissance'] = int(puissance)
	print("Votre super-héro a été ajouté à l'annuaire !")

def check_phone(numero) :
	"""
	Vérifie que le numéro de téléphone est de la forme (CCC)-CCC-CCCC où C est un chiffre.

	"""
	check = False
	if len(numero) == 14 :
		if numero[0] == '(' and numero[4] == ')' and numero[5] == '-' and numero[9] == '-' :
			if numero[1:4].isdigit() and numero[6:9].isdigit() and numero[10:14].isdigit() :
				check = True
	return check

def check_puissance(puissance) :
	"""
	Vérifie que la puissance est un entier entre 0 et 100.

	"""
	try :
		if 0 <= int(puissance) <= 100 :
			check = True
		else :
			check = False
	except ValueError :
		check = False
	return check

def filter_hero(annuaire) :
	"""
	Permet à l'utilisateur de retrouver les héros de sa ville assez puissants pour l'aider.

	"""
	ville = input("Ok, dans quelle ville vous trouvez-vous ? ")
	if not ville in annuaire :
		print("Aucun super-héro ne se trouve dans cette ville... Bonne chance !")
	else :
		x = len(annuaire[ville])
		print("Il y a {} super-héro(s) dans cette ville.".format(x))
		puissance_minimale = input("Quel niveau de puissance minimum avec-vous besoin ? ")
		while not check_puissance(puissance_minimale) :
			print("Vous devez rentrer un nombre entier entre 0 et 100 !")
			puissance_minimale = input("Quel niveau de puissance minimum avec-vous besoin ? ")
		heros_puissants = []
		for hero in annuaire[ville] :
			if annuaire[ville][hero]['puissance'] >= int(puissance_minimale) :
				heros_puissants.append(hero)
		if len(heros_puissants) == 0 :
			print("Aucun des super-héros n'est suffisamment puissant... Bonne chance !")
		else :
			print()
			print("Voici le(s) super-héro(s) que vous pouvez contacter :")
			for i in heros_puissants :
				print_hero(i, ville, annuaire)
			print('\n')

def print_hero(hero, ville, annuaire,r = '') :
	""" 
	Affiche le profil du héro demandé.

	"""
	print()
	print("{0}, possédant une puissance de {1}.".format(hero,annuaire[ville][hero]['puissance']))
	print("Son/ses super-pouvoir(s) :")
	for i in annuaire[ville][hero]['supers pouvoirs'] :
		print("- {}".format(i))
	print("Numéro de téléphone : {}".format(annuaire[ville][hero]['telephone']))

def print_annuaire(annuaire) :
	"""
	Affiche l'annuaire.

	"""
	print()
	for ville in annuaire :
		print("Dans la ville de {}, il y a le(s) super-héro(s) suivant(s) : ".format(ville))
		for hero in annuaire[ville] :
			print_hero(hero,ville,annuaire)
		print('\n')

def menu(fichier) :
	"""
	Gère l'accès aux différentes options de l'annuaire.

	"""
	print("Bienvenue dans le programme de contact des super-héros !")
	annuaire = read_data(fichier) #charge l'annuaire avec les héros du fichier
	if not annuaire : #vérifie si l'annuaire est vide
		print("Vous n'avez pas de super-héros actuellement dans l'annuaire,")
		print("veuillez effectuer au moins une entrée : ")
		add_hero(annuaire)
	choix = '0'
	dict_menu = {'1' : add_hero, '2' : filter_hero, '3' : print_annuaire, '4' : write_data}
	while choix != '5' :
		print("Que voulez-vous faire ?")
		print("1) Ajouter un nouveau super-héro à l’annuaire")
		print("2) Contacter un super-héro, je suis en danger !")
		print("3) Voir l’annuaire")
		print("4) Sauvegarder l’annuaire")
		print("5) Quitter le programme")
		choix = input()
		while choix not in ['1','2','3','4','5'] :
			print("Vous devez choisir 1, 2, 3, 4 ou 5 !")
			choix = input()
		if choix == '5' :
			print("Merci d'avoir utilisé notre programme de contact !")
			sys.exit()
		else :
			dict_menu[choix](annuaire)

if __name__ == "__main__" :
	if sys.argv[1] == "example_annuaire.txt" : 
		menu("example_annuaire.txt")
	else :
		print("Le fichier '{}' n'existe pas".format(sys.argv[1]))
		sys.exit()
		#arrête le programme si le mauvais fichier est fourni